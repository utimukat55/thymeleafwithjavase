package ga.kasukasu.poc.ThymeleafWithJavaSe;

import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IContext;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

/**
 * sample.Main of https://tadashi.hatenablog.com/entry/2019/02/22/115743 .
 *
 */
public class App {
	public static void main(String[] args) throws Exception {
		new App().execute(args);
	}

	private void execute(String[] args) throws Exception {
		// テンプレートエンジンを初期化する
		final TemplateEngine engine = initializeTemplateEngine();
		// コンテキストを生成する
		final IContext ctx = makeContext(args);
		// 今回はWriter経由で結果を出力するのでWriterも初期化
		final Writer writer = new FileWriter("sample.html");
		// テンプレート名とコンテキストとWriterを引数としてprocessメソッドをコール
		engine.process("sample", ctx, writer);
		// Writerをクローズ
		writer.close();

	}

	private TemplateEngine initializeTemplateEngine() {
		// エンジンをインスタンス化
		final TemplateEngine templateEngine = new TemplateEngine();
		// テンプレート解決子をインスタンス化（今回はクラスパスからテンプレートをロードする）
		final ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
		// Template Mode 'XHTML' is deprecated. Using Template Mode 'HTML' instead.
		// テンプレートモードはXHTML
		//resolver.setTemplateMode("XHTML");
		// テンプレートモードはHTML
		resolver.setTemplateMode("HTML");
		// クラスパスのtemplatesディレクトリ配下にテンプレートファイルを置くことにする
		resolver.setPrefix("templates/");
		// テンプレートの拡張子はhtml
		resolver.setSuffix(".html");
		// テンプレート解決子をエンジンに設定
		templateEngine.setTemplateResolver(resolver);
		return templateEngine;
	}

	private IContext makeContext(String[] args) {
		final IContext ctx = new Context();
		// 変数マップにテンプレート変数を設定

		String[] values = {"aaa", "bbb"};
		((Context) ctx).setVariable("args", values);

		//公式ガイドの日本語訳がこれだったのでそのままコピーした…
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
		Calendar cal = Calendar.getInstance();
		((Context) ctx).setVariable("today", dateFormat.format(cal.getTime()));
		return ctx;
	}
}
